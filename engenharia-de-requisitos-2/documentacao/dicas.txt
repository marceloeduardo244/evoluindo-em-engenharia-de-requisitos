** Construinfo documento de requisitos ** 

O documento de requisitos é um dos principais artefatos que são gerados ao longo do processo de engenharia de requisitos.

Não existe um modelo padrão de documento de requisitos, mas em geral eles costumam seguir o mesmo padrão que vimos.

Existem inúmeras discussões sobre a importância e a necessidade da documentação de requisitos. O fato é que existe sim a necessidade dessa documentação entretanto ela não precisa ser longa e tão pouco rígida mas deve ser simples, direta e que traga e esclareça as principais dúvidas do cliente e da equipe de desenvolvimento.

Links uteis:
- https://blog.db1group.com/5-dicas-para-a-sua-documentacao-de-requisitos-gerar-valor/
- https://blog.geekhunter.com.br/qual-e-a-importancia-da-documentacao-de-software/

# Ajustes
- Ao validar o documento com o cliente, ele pode pedir ajustes.
    - Devemos esclarecer a duvida com o cliente
    - Reescrever o requisito
    - Submeter a aprovação novamente com o cliente

** Assim que o cliente valida o documento, ele vai para o time de dev